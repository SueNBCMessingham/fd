﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrller : MonoBehaviour {

	[SerializeField] GameObject player;      

    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

  
    void Start () 
    {
      
        offset = transform.position - player.transform.position;
    }
    

    void LateUpdate () 
    {

        transform.position = player.transform.position + offset;
    }
}
