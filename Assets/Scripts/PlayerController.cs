﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	
	[SerializeField] float speed;             

    private Rigidbody2D rb2d;       


    void Start()
    {
       
        rb2d = GetComponent<Rigidbody2D> ();
		speed = 12;
    }


    void FixedUpdate()
    {
      
        float moveHorizontal = Input.GetAxis ("Horizontal");


        float moveVertical = Input.GetAxis ("Vertical");
		
        Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

       
        rb2d.AddForce (movement * speed);
    }

	void OnTriggerEnter2D(Collider2D other) 
    {
       Debug.Log (other.name);
        if (other.gameObject.CompareTag("PickUp"))
                {
                     other.gameObject.SetActive(false);
                }
    }
}
